import random
from random import choice, randint
from btu.data import b_names, g_names, last_names, subject


l = []  # სტუდენტების სრული სახელი და გვარი
for i in range(500):
    b = (random.choice(b_names) + " " + random.choice(last_names))
    g = (random.choice(g_names) + " " + random.choice(last_names))
    l.append(b)
    l.append(g)


l1 = []  # სტუდენტები, მათი საგნები და ქულები
for i in range(100000):
    full_name_students = {'full_name': choice(l), 'subject': choice(subject), 'score': randint(1, 100)}
    l1.append(full_name_students)
# print(l1)  # კომენტარიდან მოხსენი თუ გინდა რომ მთლიანი დიქშინერიც დაპრინტოს

l2 = []
for full_name_students in l1:
    students_info = filter(
        lambda dic: dic['full_name'] == full_name_students['full_name'],
        l1
    )

    d = {}  # ქულები საგნის მიხედვით
    for k in students_info:
        if k['subject'] not in d:
            d[k['subject']] = []

        if k['subject'] in d:
            d[k['subject']].append(k['score'])

    students_info_merge = {'full_name': full_name_students['full_name'], 'subject': d}
    l2.append(students_info_merge)


l3 = []  # ქულების საშუალო არითმეტიკული
for full_name_students in l2:
    full_name_students['avg'] = {}

    for b, s in full_name_students['subject'].items():
        full_name_students['avg'][b] = sum(s) / len(s)
    l3.append(full_name_students)


l4 = []  # gpa ს გამოთვლა (l3 ის  საშუალო)
for full_name_students in l3:
    full_name_students['gpa'] = sum(full_name_students['avg'].values()) / len(full_name_students['avg'].values())
    l4.append(full_name_students)


l5 = {'gpa': 0}  # ყველაზე მაღალი gpa ს მქონე სტუდენტი
for full_name_students in l4:
    if full_name_students['gpa'] > l5['gpa']:
        l5['gpa'] = full_name_students['gpa']
        l5['full_name'] = full_name_students['full_name']

print("უმაღლესი GPA აქვს : ", l5['full_name'] + "ს", " GPA :", l5['gpa'])